// Inspired from https://raw.githubusercontent.com/ccache/ccache/master/cmake/StdAtomic.cmake
// The project from which it derives is licensed under GPL-3.0-or-later
// From: Joel Rosdahl <joel@rosdahl.net>
// Date: Tue, 10 Nov 2020 19:43:14 +0100

#include <atomic>
int main()
{
  std::atomic<long long> x;
  (void)x.load();
  return 0;
}
